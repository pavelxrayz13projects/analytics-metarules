"""Настройки метаправил
"""
import datetime
import os


LOG_LEVEL = os.getenv('LOG_LEVEL', 'WARNING')


# Соединение с базой
DB_NAME = os.getenv('DB_NAME', 'analytics')
DB_HOST = os.getenv('DB_HOST', 'localhost')
DB_PORT = os.getenv('DB_PORT', '27017')


# Сервис инцедентов

INCIDENTS_TEMPLATES_URL = os.getenv('INCIDENTS_TEMPLATES_URL')
INCIDENTS_URL = os.getenv('INCIDENTS_URL', 'http://localhost:8000/api/tickets/create')
INCIDENTS_GET_URL = 'http://localhost:8100/incidents/short/?created={}'
INCIDENTS_USER = os.getenv('INCIDENTS_USER')
INCIDENTS_PASSWORD = os.getenv('INCIDENTS_PASSWORD')

# Для классификатора
CLASSIFIER_ADDRESSES_TO_IGNORE = [
    '91.244.183.5',
    '91.244.183.251',
    '91.244.183.20',
    '91.244.183.29'
]

CLASSIFIER_DELAY = int(os.getenv('CLASSIFIER_DELAY', '120'))
CLASSIFIER_BACKWARD = datetime.timedelta(seconds=int(os.getenv('CLASSIFIER_DELAY', '45')))
CLASSIFIER_FORWARD = datetime.timedelta(seconds=CLASSIFIER_DELAY)
CLASSIFIER_INCIDENT_INTERVAL = datetime.timedelta(hours=6)

CLASSIFIER_LOW_INCIDENT_SCORE = int(os.getenv('CLASSIFIER_LOW_INCIDENT_SCORE', 8))
