"""Основные модели для правил"""
import datetime
import json
import logging
import socket

import mongoengine
import requests

import const
import settings
from utils import prepare_ticket


logger = logging.getLogger(__name__)


class Rule(mongoengine.Document):
    """
    Исходный текст правила
    """
    _id = mongoengine.StringField(verbose_name='Идентификатор правила')

    text = mongoengine.StringField(verbose_name='Исходный текст правила')

    meta = {
        'collection': 'rules',
        'strict': False
    }


class Event(mongoengine.Document):
    """
    События с IDS
    """
    _id = mongoengine.ObjectIdField()

    sensor_ip = mongoengine.StringField(verbose_name='IP-адрес сенсора')

    logdate = mongoengine.DateTimeField(verbose_name='Время события')

    rule = mongoengine.ReferenceField(Rule, verbose_name='Правило')

    priority = mongoengine.IntField(verbose_name='Важность атаки')

    event_id = mongoengine.StringField(verbose_name='Идентификатор события')

    sensor_id = mongoengine.StringField(verbose_name='Идентификатор сенсора')

    src = mongoengine.StringField(verbose_name='IP-адрес источника')

    spt = mongoengine.IntField(verbose_name='Порт источника')

    smac = mongoengine.StringField(verbose_name='MAC-адрес источника')

    dst = mongoengine.StringField(verbose_name='IP-адрес назначения')

    dpt = mongoengine.IntField(verbose_name='Порт назначения')

    dmac = mongoengine.StringField(verbose_name='MAC-адрес назначения')

    proto = mongoengine.StringField(verbose_name='Протокол')

    sig_text = mongoengine.StringField(verbose_name='Сообщение сигнатуры')

    tags = mongoengine.StringField(verbose_name='Метки правила')

    homenet_address = mongoengine.ListField(
        mongoengine.StringField(),
        verbose_name='Адреса из домашней сети',
    )

    meta = {
        'collection': 'analytic_logs',
        'strict': False,
        'indexes': [
            '$sig_text',
            {'fields':['-logdate'],'expireAfterSeconds':3888000}
        ]
    }

    def __str__(self):
        return '{} {} {}'.format(self._id, self.logdate, self.sig_text)

    @staticmethod
    def get_latest_from(object_id):
        meta_events = Event.objects.filter(_id__gt=object_id,
                                           tags=const.TAG_META)

        classifier_events = Event.objects.filter(_id__gt=object_id,
                                                 tags__in=const.TAG_CLASSIFY)

        return meta_events, classifier_events

    @staticmethod
    def get_latest_object_id():
        latest_event = Event.objects.order_by('-_id').first()
        return latest_event._id

    def count_previous(self, window, sig_tags):
        """
        Посчитать количество сработавших сигнатур по хосту с тэгами
        :param window: Временное окно
        :param sig_tags: Список тэгов по которым суммировать
        :return: Количество сработавших сигнатур
        """
        filters = {
            'homenet_address': self.homenet_address,
            'tags__all': sig_tags,
            'sensor_id': self.sensor_id,
            'logdate__gte': self.logdate - datetime.timedelta(seconds=window),
            'logdate__lte': self.logdate,
        }

        events = Event.objects.filter(**filters)
        counter = len(events)
        correlated_events = events[1:]
        return counter, correlated_events

    def get_correlated_events(self, period, text):
        """
        Получить список кореллированных событий
        :param period: временной интервал в секундах
        :param text: Список слов, кроторые нужно искать
        :return:
        """
        filters = {
            'homenet_address': self.homenet_address,
            'logdate__gte': self.logdate - datetime.timedelta(seconds=period),
            'logdate__lte': self.logdate,
            'sensor_id': self.sensor_id,
            'event_id__ne': self.event_id,
        }
        correlated_events = Event.objects.filter(**filters)

        if not text:
            return correlated_events

        filtered_correlated_events = []
        for event in correlated_events:
            if any(word in event.sig_text for word in text.split()):
                filtered_correlated_events.append(event)

        return filtered_correlated_events

    def get_fields_for_incident(self):
        event = {
            'date_fact': self.logdate,
            'message': self.sig_text,
            'src_address': '{}:{}'.format(self.src, self.spt),
            'src_mac': self.smac,
            'dst_address': '{}:{}'.format(self.dst, self.dpt),
            'dst_mac': self.dmac,
            'pcap_link': 'http://{}/service/pcap_payload?id={}'.format(
                self.sensor_ip, self.event_id),
            'event_id': self.event_id,
        }
        return event

    def get_affected_actives(self):
        """
        Привести адреса домашней сети к формату поля пораженных активов
        :return:
        """
        affected_actives = []
        for address in self.homenet_address:
            active = {
                'ip': address,
                'mac': self.smac if (self.src == address) else self.dmac
            }
            affected_actives.append(active)

        return affected_actives

    def get_data_for_classifier(self, date_from, date_to):
        data = ''
        filters = {
            'homenet_address': self.homenet_address,
            'logdate__gte': date_from,
            'logdate__lte': date_to,
            'sensor_ip': self.sensor_ip,
        }
        result = Event.objects.filter(**filters).only('rule').distinct('rule')

        # Для исследований
        correlated_events = Event.objects.filter(**filters, event_id__ne=self.event_id)

        for rule_ref in result:
            try:
                data += Rule.objects.get(_id=rule_ref.id).text
            except Rule.DoesNotExist:
                continue

        return data, correlated_events


class Organisation(mongoengine.Document):
    """
    Организации
    """
    name = mongoengine.StringField(verbose_name='Наименование')

    uuid = mongoengine.UUIDField(verbose_name='UUID')

    branches = mongoengine.ListField(mongoengine.DictField(),
                                     verbose_name='Структура')

    meta = {
        'collection': 'organisations',
        'strict': False,
    }

    @staticmethod
    def get_full_location(sensor_id):
        """
        Получить полные данные о местоположении и сенсоре
        :return:
        """
        full_location = {
            'org_name': '',
            'uuid': '',
            'branch': '',
            'sensor': '',
            'segment': '',
            'external_id': '',
            'ip': '',
        }
        for organisation in Organisation.objects.all():
            for branch in organisation['branches']:
                for segment in branch['segments']:
                    for sensor in segment['sensors']:
                        if sensor['external_id'] == sensor_id:
                            full_location = {
                                'org_name': organisation['name'],
                                'uuid': branch['uuid'],
                                'branch': branch['name'],
                                'segment': segment['name'],
                                'external_id': sensor['external_id'],
                                'sensor': sensor['type'],
                                'ip': sensor['address'],
                            }
                            return full_location

    @staticmethod
    def get_terminals():
        """
        Получить словарь сенсоров с терминальными серверами
        :return:
        """
        sensor_terminals = {}

        for organisation in Organisation.objects.all():
            for branch in organisation['branches']:
                for segment in branch['segments']:
                    for sensor in segment['sensors']:
                        sensor_terminals[sensor['external_id']] = sensor['terminals']

        return sensor_terminals


class Ticket(mongoengine.Document):
    """
    Карточка инцидента
    """
    location = mongoengine.DictField(verbose='Место где произошел инцидент')

    title = mongoengine.StringField(verbose_name='Наименование')

    level = mongoengine.IntField(verbose_name='Уровень критичности')

    mode = mongoengine.StringField(verbose_name='Механизм генерации карточки')

    symptoms = mongoengine.ListField(
        mongoengine.StringField(),
        verbose_name='Признаки инцидента',
    )

    threats = mongoengine.ListField(
        mongoengine.StringField(),
        verbose_name='Угрозы ИБ'
    )

    methods = mongoengine.ListField(
        mongoengine.StringField(),
        verbose_name='Классы инцидента'
    )

    created = mongoengine.DateTimeField(
        verbose_name='Дата и время обнаружения'
    )

    affected_actives = mongoengine.ListField(
        mongoengine.DictField(),
        verbose_name='Пораженные активы',
    )

    recommendations = mongoengine.ListField(
        mongoengine.StringField(),
        verbose_name='Рекомендации'
    )

    main_events = mongoengine.ListField(
        mongoengine.DictField(),
        verbose_name='Основные события'
    )

    correlated_events = mongoengine.ListField(
        mongoengine.DictField(),
        verbose_name='Связанные события',
    )

    summary = mongoengine.StringField(verbose_name='Описание инцидента')

    status = mongoengine.StringField(
        default='expected',
        verbose_name='Статус инцидента',
    )

    meta = {
        'collection': 'tickets',
        'strict': False,
        'indexes': [
            {'fields':['-created'],'expireAfterSeconds':31536000}
        ]
    }

    @staticmethod
    def is_exists(title, affected_actives, created):
        """
        Проверить, был ли уже создан этот инцидент за определенный интервал
        :param title: Наименование
        :param affected_actives: Пораженные активы
        :param created: Дата и время обнаружения
        :return:
        """
        filters = {
            'title': title,
            'affected_actives': affected_actives,
            'created__gte': created - const.INCIDENT_INTERVAL,
            'created__lte': created,
        }
        already_exists = Ticket.objects.filter(**filters).count()
        return already_exists

    def already_exist_classifier(self):
        """
        Для отладки классификатора
        :return:
        """
        filters = {
            'title': self.title,
            'level': self.level,
            'affected_actives': self.affected_actives,
            'created__gte': self.created - settings.CLASSIFIER_INCIDENT_INTERVAL,
            'created__lte': self.created,
        }
        has_same = Ticket.objects.filter(**filters).count()
        return has_same


    @staticmethod
    def create_ticket(event, template_name, correlated_events=None,
                      mode=const.META_MODE, level=None, summary=None):
        """
        Создать инцидент
        :param event: Главное событие
        :param template_name: Шаблон инцидента
        :param correlated_events: Связанные события
        :param mode: Режим генерации инцидента (мета или классификатор)
        :param level:
        :param summary:
        :return:
        """
        created = datetime.datetime.now()

        try:
            template = TicketTemplate.objects.get(name=template_name)
        except TicketTemplate.DoesNotExist:
            logger.error('Не найден шаблон {}'.format(template_name))
            return

        affected_actives = event.get_affected_actives()

        if mode == const.META_MODE:
            # Проверим, нужно ли создавать инцидент
            if Ticket.is_exists(
                    template.template.get('title'),
                    affected_actives,
                    created):
                return

        # Наполним из шаблона всё что можем
        ticket = Ticket(**template.template)

        ticket.mode = mode
        ticket.created = created
        ticket.affected_actives = affected_actives

        full_location = Organisation.get_full_location(event.sensor_id)

        ticket.location = {
            'organisation': full_location['org_name'],
            'branch': full_location['branch'],
            'uuid': full_location['uuid'],
        }

        # Пока у нас всё на одном сенсоре
        sensor = {
            'sensor': full_location['sensor'],
            'segment': full_location['segment'],
            'external_id': full_location['external_id'],
            'ip': full_location['ip'],
        }
        sensor_main_events = sensor.copy()
        sensor_main_events['events'] = [event.get_fields_for_incident()]

        ticket.main_events = [sensor_main_events]

        ticket.correlated_events = []
        if correlated_events:
            sensor_correlated_events = sensor.copy()
            sensor_correlated_events['events'] = \
                [e.get_fields_for_incident() for e in correlated_events]
            ticket.correlated_events = [sensor_correlated_events]

        if level:
            ticket.level = level
        if summary:
            ticket.summary = ticket.title
            ticket.title = summary

        if ticket.already_exist_classifier():
            return

        ticket.save()

        incident_server = IncidentServer.objects.first()
        if incident_server and incident_server.enabled:
            try:
                incident_server.send_tcp(ticket)
            except:
                logger.error('Ошибка при отправке инцидента')

        return ticket


class TicketTemplate(mongoengine.Document):
    """
    Шаблон инцидента
    """
    name = mongoengine.StringField(primary_key=True, verbose_name='Наименование')
    template = mongoengine.DictField(verbose_name='Шаблон')

    meta = {
        'collection': 'ticket_templates',
        'strict': False,
    }


class IncidentServer(mongoengine.Document):
    """
    Сервер на который передавать инциденты
    """
    host = mongoengine.StringField(verbose_name='Хост')
    port = mongoengine.IntField(verbose_name='Порт')
    enabled = mongoengine.BooleanField(verbose_name='Включен')

    meta = {
        'collection': 'incident_server',
        'strict': False,
    }

    def send_tcp(self, ticket):
        """Отправить сообщение по UDP"""
        message = prepare_ticket(ticket)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((self.host, self.port))
        sock.send(message.encode('utf-8'))
        sock.close()
