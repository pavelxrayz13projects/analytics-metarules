"""Каждые 15 секунд получать окно свежих событий
"""
import logging
import threading

import const
from metarules.aggregation import aggregation_metarule
from metarules.critical_tags import critical_tags_metarule
from rules.models import Event
from rules.pair_tags import pair_tags_metarule
from classifier import predict_return


logger = logging.getLogger(__name__)


def get_window(latest_object_id=None):
    logger.debug('get_window from {}'.format(latest_object_id))

    if not latest_object_id:
        latest_object_id = Event.get_latest_object_id()

    # Если latest_object_id так и не был получен, то база ещё пустая
    if not latest_object_id:
        threading.Timer(
            const.REFRESH_TIME,
            get_window,
            kwargs={'latest_object_id': None}
        ).start()
        return

    # Переопределим последнее событие
    new_latest_object_id = Event.get_latest_object_id()
    meta_events, classifier_events = Event.get_latest_from(latest_object_id)

    # Запустим с обновленным latest_object_id
    threading.Timer(
        const.REFRESH_TIME,
        get_window,
        kwargs={'latest_object_id': new_latest_object_id}
    ).start()

    # Если не было интересных событий - выходим
    if not meta_events and not classifier_events:
        return

    tickets = {}
    if meta_events:
        tickets.update(critical_tags_metarule(meta_events))
        tickets.update(aggregation_metarule(meta_events))
        tickets.update(pair_tags_metarule(meta_events))

    logger.debug(classifier_events)
    if classifier_events:
        predict_return.analyze(classifier_events)
