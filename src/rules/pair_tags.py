"""При срабатывании связки тэгов за указанный интервал - заводить инцедент
Должны отработать все тэги из списка с промежутком, указанном в правиле
"""
import logging
from collections import namedtuple

import const
from rules.models import Ticket


logger = logging.getLogger(__name__)

# Правила
FIELDS = ['tags_pair', 'interval', 'window', 'threshold', 'template']

# TODO: Переделать на данные из базы и перенести в модель
Rule = namedtuple('Rule', FIELDS)

rdp_ddos = Rule(
    [{'login_attempt', 'rdp'}, {'login_response', 'rdp', 'bruteforce'}],
    3,
    120,
    6,
    'rdp_ddos'
)

PAIR_TAGS_RULES = (rdp_ddos,)


def pair_tags_metarule(data):
    return {}
