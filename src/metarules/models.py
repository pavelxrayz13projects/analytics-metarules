"""Модели для метаправил"""
import mongoengine
import logging


logger = logging.getLogger(__name__)


class Metarule(mongoengine.Document):
    """
    Класс метаправил
    """
    name = mongoengine.StringField(primary_key=True)
    template = mongoengine.StringField()
    algorithm = mongoengine.StringField()
    parameters = mongoengine.DictField()

    @property
    def tags(self):
        return set(self.parameters.get('tags'))

    @property
    def window(self):
        return self.parameters.get('window')

    @property
    def threshold(self):
        return self.parameters.get('threshold')

    @property
    def words(self):
        return self.parameters.get('words')

    meta = {
        'collection': 'metarules',
        'strict': False
    }
