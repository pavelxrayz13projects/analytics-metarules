"""Метоправило выявления инцидентов с использованием агрегации
"""
import logging

import const
from metarules.models import Metarule
from rules.models import Ticket


logger = logging.getLogger(__name__)


def aggregation_metarule(data):
    tickets = {}

    for event in data:
        logger.debug(event.tags)

        if not event.homenet_address:
            logger.debug(const.ASSET_DEFINING_ERROR)
            continue

        # Применим метаправила
        for rule in Metarule.objects.filter(algorithm='aggregation'):
            # Получим параметры метаправила

            if rule.tags.issubset(event.tags):

                key = (tuple(event.homenet_address),
                       tuple(rule.tags))  # идентификатор тикета

                # Если уже было - пропускаем чтобы не запрашивать базу
                if key in tickets:
                    continue

                # Посчитаем количество срабатываний
                # Нам интересны только события с meta
                tags_to_check = rule.tags
                tags_to_check.add(const.TAG_META)
                qnty, correlated_events = event.count_previous(rule.window, tags_to_check)

                logger.debug('event id {}, window {}, tags {}'.format(event._id,
                                                                      rule.window,
                                                                      tags_to_check))
                logger.debug('правило {}'.format(rule.template))
                logger.debug('сработало:{} нужно: {}'.format(qnty, rule.threshold))

                # Если превысили, то заводим инцидент
                if qnty >= rule.threshold:
                    tickets[key] = Ticket.create_ticket(event, rule.template, correlated_events)

        logger.debug(tickets)

    return tickets
