"""При срабатывании тэга из набора - заводить инцедент
"""
import logging

import const
from metarules.models import Metarule
from rules.models import Ticket


logger = logging.getLogger(__name__)


def critical_tags_metarule(data):
    tickets = {}
    for event in data:
        logger.debug(event.tags)

        if not event.homenet_address:
            logging.error(const.ASSET_DEFINING_ERROR)
            continue

        key = ()
        for rule in Metarule.objects.filter(algorithm='critical'):
            if rule.tags.issubset(event.tags):
                key = (tuple(event.homenet_address), tuple(rule.tags))  # идентификатор тикета

            if not key or key in tickets:
                continue

            correlated_events = event.get_correlated_events(rule.window,
                                                            rule.words)
            tickets[key] = Ticket.create_ticket(event,
                                                rule.template,
                                                correlated_events)
    return tickets
