"""Константы для метаправил
"""
import datetime


OK_STATUS = 200
CONNECTION_TIMEOUT = 10
RETRY_TIMER = 30

TAG_META = 'meta'
TAG_CLASSIFY = [
    'classify',
    'malware',
    'exploitation_response',
    'exploitation_attempt',
]

# Константы для инцидентов

# Интервал повторного заведения инцидента
INCIDENT_INTERVAL = datetime.timedelta(hours=12)

META_MODE = 'metarules'  # Режим создания инцидента метаправилами
CLASSIFIER_MODE = 'classifier'  # Режим создания инцидента классификатором
CLASSIFIER_TEMPLATE_10_09 = 'classifier_10_09'
CLASSIFIER_TEMPLATE_08_07 = 'classifier_08_07'

# Log constants

FORMAT = "%(asctime)s %(levelname)s %(module)s %(message)s"

# messages

ERROR_NO_BASE = 'Ошибка. Не задана база Elasticsearch'
ERROR_NO_INDEX = 'Ошибка. Не задан шаблон индекса Elasticsearch'
ERROR_NO_HOMENET = 'Ошибка. Не указана сеть Homenet'
ERROR_HOMENET_JSON = 'Ошибка. Синтаксическая ошибка в параметре Homenet.'
ERROR_HOMENET = 'Ошибка в параметре Homenet.'
CONNECTION_ERROR = 'Ошибка подключения к базе Elasticsearch'
SERVER_STATUS_ERROR = 'Сервер Elasticsearch вернул ошибку с кодом {}'
REQUEST_ERROR = 'Ошибка при запросе к базе: {} {} {}'
ASSET_DEFINING_ERROR = 'Ошибка определения актива. Ни один из адресов (src, dst) не находится в сети Homenet'
ERROR_SENSOR_HAS_NO_HOMENET = 'Ошибка. Сенсору {} не задан Homenet'
PKUIB_SERVICE_ERROR = 'Сервис отправки в ПКУИБ вернул ошибку с кодом {}'
PKUIB_SERVICE_CONNECTION_ERROR = 'Не удалось переслать инцедент в сервис отправки в ПКУИБ'

# Размер окна событий метаправил
REFRESH_TIME = 15

# Для писем
INCIDENT_MANAGER_URL = 'http://10.0.24.200:8080#/incidents/{}'  # Только для демо
MAIL_SUBJECT = 'Зафиксирован инцидент'
MAIL_TEMPLATE = '''Зафиксирован инцидент: {title}
Рейтинг: {level}
Подробная информация по ссылке: {url}
'''
