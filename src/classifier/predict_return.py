import logging
import pickle
import threading

import pandas as pd
import numpy as np

import const
import settings
from rules.models import Ticket, Organisation


logger = logging.getLogger(__name__)


def read_models():
    """
    Чтение моделей
    """
    vectorizer = pickle.load(open('/opt/models/vectorizer.p', 'rb'))
    classifier_model = pickle.load(open('/opt/models/classifier.p', 'rb'))
    samples_clusters_dict = pd.read_json(path_or_buf='/opt/models/samples_clusters_dict.json')
    return vectorizer, classifier_model, samples_clusters_dict


def predict_class(text, vectorizer, classifier_model, samples_clusters_dict):
    x = vectorizer.transform(np.array([text]))
    y = classifier_model.predict(x)
    proba = classifier_model.predict_proba(x)

    cho_proba = np.empty(y.shape)

    for index, item in enumerate(y):
        cho_proba[index] = proba[index][item]

    return [samples_clusters_dict['cluster_name'][samples_clusters_dict['cluster_number'] == int(i)].iloc[0] for i in y], y, cho_proba


def analyze_event(event):
    logger.debug('analyze_event {}'.format(event.event_id))

    data, correlated_events = event.get_data_for_classifier(
        event.logdate - settings.CLASSIFIER_BACKWARD,
        event.logdate + settings.CLASSIFIER_FORWARD,
    )

    result = predict_class(data, vectorizer, classifier_model,
                           samples_clusters_dict)

    level = round(result[-1][0]*10)
    class_id = str(result[1][0])
    class_name = result[0][0].lower()
    summary = '{} {}'.format(class_name, class_id)

    logger.debug('result {} on {}'.format(result, event.homenet_address))

    if level >= 9:
        template = const.CLASSIFIER_TEMPLATE_10_09
    elif level >= settings.CLASSIFIER_LOW_INCIDENT_SCORE:
        template = const.CLASSIFIER_TEMPLATE_08_07
    else:
        return

    # заведём инцидент, если нужно
    Ticket.create_ticket(event, template,
                         mode=const.CLASSIFIER_MODE,
                         level=level, summary=summary,
                         correlated_events=correlated_events)


def analyze(data):
    """
    Запуск анализа окна событий
    В настройках имеется список хостов, на которые классификатор не запускается
    Классификатор не должен запускаться на один хост в одном окне
    """
    hosts_to_ignore = []
    sensor_terminals = Organisation.get_terminals()
    for event in data:
        # Некоторые ресурсы нам нужно пропускать
        if event.src in hosts_to_ignore:
            continue
        elif event.dst in hosts_to_ignore:
            continue

        # Пропустим терминальные серверы
        external_id = event.sensor_id
        terminal_servers = sensor_terminals.get(external_id, [])
        if (event.src in terminal_servers) or (event.dst in terminal_servers):
            continue

        # Классификация запускается один раз на один комп в одном окне
        hosts_to_ignore.extend(event.homenet_address)

        threading.Timer(
            settings.CLASSIFIER_DELAY,
            analyze_event,
            kwargs={'event': event}
        ).start()


vectorizer, classifier_model, samples_clusters_dict = read_models()
