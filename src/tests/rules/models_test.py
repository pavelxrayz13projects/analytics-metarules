import unittest
from datetime import datetime

from mongoengine import connect

from rules.models import Event, Ticket


CONNECT = connect()

# result = Event.get_latest_from(ObjectId('571e32d4d676c012884dd754'))
# print(result[0].rule.text)

# event_id = Event.get_latest_object_id()
#
# event = Event.objects.get(_id=ObjectId('571e331ad676c012884dd765'))
#
# print(event.sig_text)
#
# counter = Event.count_previous(event, 1000, ['info', 'policy'])
# print(counter)



class TestEvent(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.event = None

    def setUp(self):
        self.event = Event(
            homenet_address=['127.0.0.1', '127.0.0.2'],
            src='127.0.0.1',
            smac='mac1',
            dst='127.0.0.2',
            dmac='mac2',
        )

    def test_get_affected_actives(self):
        affected_actives = [
            {
                'ip': '127.0.0.1',
                'mac': 'mac1',
            },
            {
                'ip': '127.0.0.2',
                'mac': 'mac2',
            }
        ]

        self.assertEqual(self.event.get_affected_actives(), affected_actives)


class TestTickets(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title = 'test_title'
        self.affected_actives = [{'ip': '127.0.0.1', 'mac': 'mac1'}]
        self.created = datetime.now()

    def setUp(self):
        ticket = Ticket(
            title=self.title,
            affected_actives=self.affected_actives,
            created=self.created,
        )
        ticket.save()

    def test_is(self):
        exists = Ticket.is_exists(
            title=self.title,
            affected_actives=self.affected_actives,
            created=datetime.now(),
        )
        print(exists)

        exists = Ticket.is_exists(
            title='new_title',
            affected_actives=self.affected_actives,
            created=datetime.now(),
        )
        print(exists)

    def tearDown(self):
        tickets = Ticket.objects.all()

        for ticket in tickets:
            ticket.delete()
