from mongoengine import connect
from mongoengine.fields import ObjectId

connect('analytics', host='10.0.9.118')

from rules.models import Event, Ticket
event = Event.objects.get(_id=ObjectId('573b1b0c1d41c80001998c6e'))

counter = event.count_previous(600, {'login_attempt', 'ssh', 'bruteforce'})

print(counter)
