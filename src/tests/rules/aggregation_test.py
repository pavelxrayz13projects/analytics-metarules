import unittest

from bson.json_util import loads
from mongoengine import connect
from mongoengine.fields import ObjectId

from rules.aggregation import aggregation_metarule
from rules.models import Event


class TestAggregation(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connect = connect('test')

    def setUp(self):

        with open('../fixtures/test_aggregation.json') as fixtures_file:
            fixtures = fixtures_file.read()

        test_data = [Event(**x) for x in loads(fixtures)]


        Event.objects.insert(test_data)

        oid = ObjectId('571e3310d676c012884dd700')
        self.window = Event.get_latest_from(oid)

    def test_aggregation_metarule(self):
        aggregation_metarule(self.window)

    def tearDown(self):
        self.connect.drop_database('test')
