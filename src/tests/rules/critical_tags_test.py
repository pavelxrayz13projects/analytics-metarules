import unittest

from bson.json_util import loads
from mongoengine import connect
from mongoengine.fields import ObjectId

from rules.critical_tags import critical_tags_metarule
from rules.models import Event, TicketTemplate


class TestAggregation(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.connect = connect('test')

    def setUp(self):
        TicketTemplate.get_templates(url='http://10.0.24.107:8000/templates/')

        with open('../fixtures/test_critical.json') as fixtures_file:
            fixtures = fixtures_file.read()

        test_data = [Event(**x) for x in loads(fixtures)]


        Event.objects.insert(test_data)

        oid = ObjectId('571e3310d676c012884dd700')
        self.window = Event.get_latest_from(oid)

    def test_aggregation_metarule(self):
        critical_tags_metarule(self.window)

    def tearDown(self):
        # self.connect.drop_database('test')
        pass
