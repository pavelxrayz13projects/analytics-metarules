"""Запуск получения окна данных лога и обработки его метаправилами
"""
import logging
from mongoengine import connect

import const
import settings
from rules.logwindow import get_window


logger = logging.getLogger(__name__)


def get_connection():
    host = settings.DB_HOST
    db = settings.DB_NAME
    try:
        port = int(settings.DB_PORT)
    except (TypeError, ValueError):
        port = None

    return connect(db=db, host=host, port=port)


def main():
    """Получим настройки из среды, проверим базу и запусти всё
    """
    logging.basicConfig(format=const.FORMAT,
                        level=getattr(logging, settings.LOG_LEVEL))
    connection = get_connection()
    if not connection:
        return

    get_window()


if __name__ == '__main__':
    main()
