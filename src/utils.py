"""Небольшие сторонние функции
"""
import json
import logging
import threading
from ipaddress import IPv4Address, IPv4Network, AddressValueError
from datetime import datetime

import const
import settings


logger = logging.getLogger(__name__)
# mail_client = Mailclient(subject=const.MAIL_SUBJECT)


class DatetimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%dT%H:%M:%S.%fZ')
        return json.JSONEncoder.default(self, obj)


def prepare_ticket(ticket):
    """
    Преобразовать инцидент в данные для отправки
    """
    ticket_dict = ticket.to_mongo().to_dict()
    ticket_dict.pop('_id', None)
    data = json.dumps(ticket_dict, cls=DatetimeEncoder)
    return data
