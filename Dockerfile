FROM python:3.5.1

RUN mkdir -p /opt/src
WORKDIR /opt/src

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY ./src /opt/src
COPY entrypoint.sh /tmp/entrypoint.sh
RUN chmod +x /tmp/entrypoint.sh

CMD ["/tmp/entrypoint.sh"]
